'use strict';

export default class Viewport {
	constructor(options) {

		this.currentPoint = null;
		this.winWith = null;
		this.options = options;

		if (document.readyState === 'loading') {
			document.addEventListener('DOMContentLoaded', () => {
				this.__init();
			});
		} else {
			this.__init();
		}
	}


	__init(options){

		if ( this.options ){
			this.options['Infinity'] = null;

			this.__setViewPort();

			window.onresize = ()=>{
				this.__setViewPort();
			}
		}
	}

	update(){
		this.currentPoint = null;
		let sbw = 0;
		this.winWith = window.innerWidth + sbw;
		this.__checkCurrentViewport();
	}

	__setViewPort(){
		let sbw = 0;
		this.winWith = window.innerWidth + sbw;
		this.__checkCurrentViewport();
	}

	__checkCurrentViewport(){

		let point = 0, callback = null;

		for (let key in this.options) {

			if ( key > this.winWith ){
				if ( point !== this.currentPoint ) {
					this.currentPoint = point;
					callback();
				}
				return false;
			}
			point = key; callback = this.options[key];
		}

	}

	__scrollBarWidth(){
		let scrollDiv = document.createElement('div'), sbw;
		scrollDiv.className = 'scroll_bar_measure';
		$(scrollDiv).css({
			width: '100px',
			height: '100px',
			overflow: 'scroll',
			position: 'absolute',
			top: '-9999px'
		});
		document.body.appendChild(scrollDiv);
		sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
		document.body.removeChild(scrollDiv);
		return sbw;
	}
}
