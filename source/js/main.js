import '../css/style.scss'

import $ from 'jquery'
import Tab from './modules/module.tab'
import Popup from './modules/module.popup'
import PageNav from './modules/module.pagenav'

import Inputmask from 'inputmask'

import ViewPort from './modules/module.viewport'

import ObjectFit from './modules/module.objectfit'
import 'slick-carousel'
import Viewer from 'viewerjs'


window.app = window.app || {};


$(function () {

	new ObjectFit({imageContainer: '.obj-fit'});

	function scrollBarWidth(){
		let scrollDiv = document.createElement('div'), sbw;
		scrollDiv.className = 'scroll_bar_measure';
		$(scrollDiv).css({
			width: '100px',
			height: '100px',
			overflow: 'scroll',
			position: 'absolute',
			top: '-9999px'
		});
		document.body.appendChild(scrollDiv);
		sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
		document.body.removeChild(scrollDiv);
		return sbw;
	}


	$('input[type = tel]').each(function () {
		let inputmask = new Inputmask({
			mask: '+7 (999) 999-99-99',
			showMaskOnHover: false
		});
		inputmask.mask($(this)[0]);
	});

	window.app.popup = new Popup({
		bodyClass: 'is-blur',
		onPopupOpen: (popup)=>{
			try {$(popup).find('.gallery__list').slick('setPosition');} catch (e){};
		}
	});

	 new Tab({
		onTabChange: (tab)=>{
			try {$(tab).find('.gallery__list').slick('setPosition');} catch (e){}
		}
	});

	new PageNav({
		heightElements: [],
		slideTime: 0,
		activeClass: 'is-active',
		menu: '.nav__menu',
		offset: 20
	});

	let viewPort = new ViewPort({
		'0': ()=>{
			window.app.isMobile = true;
			window.app.isDesktop = false;
			window.app.viewPort = 0;
			methodSlick();
			whoSlick();
		},
		'1260': ()=>{
			window.app.isMobile = false;
			window.app.isDesktop = true;
			window.app.viewPort = 1;
			methodSlick(true);
			whoSlick(true);
		},
		'1660': ()=>{
			window.app.isMobile = false;
			window.app.isDesktop = true;
			window.app.viewPort = 2;
			methodSlick(true);
			whoSlick(true);
		}
	});

	let $b = $('body');

	$b

		.on('click', '[data-scrollto]', function (e) {
			e.preventDefault();
			$('html,body').animate({scrollTop:$($(this).data('scrollto')).offset().top - $('.header').height() }, 500);
		})
		.on('click', '.nav__handle', function (e) {
			$b.toggleClass('is-menu-open');
		})

		.on('click', '.who__item-more', function (e) {
			e.preventDefault();
			let $t = $(this),
				$wrap = $t.closest('.who__item-info')
			;

			$wrap.toggleClass('show-more');
		})

		.on('click', '.nav__menu-item', function (e) {
			$b.removeClass('is-menu-open');
		})


		.on('blur', '.form__input input, .form__input textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');

			if ( !$.trim($input.val()) ){
				$field.removeClass('filled');
			} else {
				$field.addClass('filled');
			}
		})

		.on('submit', '.form', function (e) {
			let $form = $(this),
				wrong = false
			;

			$form.find('.form__field').removeClass('error').find('.form__message').html('');

			$form.find('input').each(function () {
				let $input = $(this),
					$field = $input.closest('.form__field'),
					$message = $field.find('.form__message')
				;

				if ( !$.trim($input.val()) ){
					wrong = true;
					$field.addClass('error');
					$message.html('Заполните поле');
				} else if ( $input.val().indexOf('_') >= 0 ){
					wrong = true;
					$field.addClass('error');
					$message.html('Мы не уверены, что это поле заполнено полностью');
				}
			});

			if ( wrong ){
				e.preventDefault();
				return false;
			}

		})

	;






	$('.staff__list').each(function () {
		let $slider = $(this),
			$wrap = $slider.parent(),
			$control = $wrap.find('.slider-control')
		;

		if ( !$control.length ) return;

		$slider
			.on('init', function (event, slick) {
				$wrap.find('.slider-control__count')
					.html((slick.currentSlide+1) + '/' + slick.slideCount);
				if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				$wrap.find('.slider-control__count')
					.html((nextSlide+1) + '/' + slick.slideCount);
			})
			.on('breakpoint', function (event, slick) {
				if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');
			})
			.slick({
				dots: false,
				infinite: false,
				swipeToSlide: true,
				speed: 300,
				prevArrow: $control.find('.slider-control__arrow_prev'),
				nextArrow: $control.find('.slider-control__arrow_next'),
				variableWidth: true,
				slidesToScroll: 1,
				mobileFirst: true,

				responsive: [
					{
						breakpoint: 1260,
						settings: {
							variableWidth: false,
							adaptiveHeight: false,
							slidesToShow: 3
						}
					},
					{
						breakpoint: 1660,
						settings: {
							variableWidth: false,
							adaptiveHeight: false,
							slidesToShow: 4
						}
					}
				]
			});


	});



	$('.member__list').each(function () {
		let $slider = $(this),
			$wrap = $slider.parent(),
			$control = $wrap.find('.slider-control')
		;

		if ( !$control.length ) return;

		$slider
			.on('init', function (event, slick) {
				$wrap.find('.slider-control__count')
					.html((slick.currentSlide+1) + '/' + slick.slideCount);
				if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');

			})
			.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
				$wrap.find('.slider-control__count')
					.html((nextSlide+1) + '/' + slick.slideCount);
			})
			.on('breakpoint', function (event, slick) {
				if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');
			})
			.slick({
				dots: false,
				infinite: false,
				swipeToSlide: true,
				speed: 300,
				prevArrow: $control.find('.slider-control__arrow_prev'),
				nextArrow: $control.find('.slider-control__arrow_next'),
				variableWidth: true,
				slidesToScroll: 1,
				mobileFirst: true,

				responsive: [
					{
						breakpoint: 1260,
						settings: {
							variableWidth: false,
							adaptiveHeight: false,
							slidesToShow: 3
						}
					},
					{
						breakpoint: 1660,
						settings: {
							variableWidth: false,
							adaptiveHeight: false,
							slidesToShow: 4
						}
					}
				]
			});


	});

	$('.schedule__list').each(function () {
		let $slider = $(this),
			$wrap = $slider.closest('.schedule'),
			$period = $wrap.find('.schedule__period-time'),
			periodIndex = []
		;

		$period.each(function (i) {
			let $p = $(this);

			if ($p.hasClass('schedule__period-time_2')){
				periodIndex.push(i);
				periodIndex.push(i);
			} else if ($p.hasClass('schedule__period-time_3')){
				periodIndex.push(i);
				periodIndex.push(i);
				periodIndex.push(i);
			} else {
				periodIndex.push(i)
			}
		});

		console.info(periodIndex);


		$slider
			.slick({
				dots: false,
				infinite: false,
				speed: 300,
				slidesToShow: 1,
				slidesToScroll: 1,
				adaptiveHeight: true,
				prevArrow: $wrap.find('.slider-control__arrow_prev'),
				nextArrow: $wrap.find('.slider-control__arrow_next'),
				mobileFirst: true,
				swipeToSlide: true,
				responsive: [
					{
						breakpoint: 1260,
						settings: {
							slidesToShow: 5
						}
					},
					{
						breakpoint: 1660,
						settings: {
							slidesToShow: 7
						}
					}
				]
			});


		$wrap
			.on('mouseover', '.schedule__item', function (e) {
				let $t = $(this),
					$parent = $t.closest('.schedule__column'),
					$sibl = $parent.find('.schedule__item'),
					$wrap = $t.closest('.schedule'),
					ind = $sibl.index($t),
					$period = $wrap.find('.schedule__period').find('.schedule__period-time')
				;

				$wrap.find('.schedule__column').each(function () {
					$(this).find('.schedule__item').eq(ind).addClass('is-hover');
				});

				$period.eq(periodIndex[ind]).addClass('is-hover');

			})

			.on('mouseleave', '.schedule__item', function (e) {
				let $t = $(this),
					$wrap = $t.closest('.schedule')
				;

				$wrap.find('.is-hover').removeClass('is-hover');

			})
	});


	$('.gallery__list').each(function () {
		let $slider = $(this),
			$wrap = $slider.parent(),
			$control = $wrap.find('.slider-control'),
			inPopup = $slider.closest('.popup').length
		;


		if ( inPopup ){

			$slider
				.on('init', function (event, slick) {
					$wrap.find('.slider-control__count')
						.html((slick.currentSlide+1) + '/' + slick.slideCount);

					if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');

					if ( slick.slideCount > slick.options.slidesToShow){
						$control.removeClass('is-hidden');
					} else {
						$control.addClass('is-hidden');
					}
				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
					$wrap.find('.slider-control__count')
						.html((nextSlide+1) + '/' + slick.slideCount);
				})
				.on('breakpoint', function (event, slick) {
					if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');
					if ( slick.slideCount > slick.options.slidesToShow){
						$control.removeClass('is-hidden');
					} else {
						$control.addClass('is-hidden');
					}
				})
				.slick({
					dots: false,
					infinite: false,
					speed: 300,
					prevArrow: $wrap.find('.slider-control__arrow_prev'),
					nextArrow: $wrap.find('.slider-control__arrow_next'),
					variableWidth: false,
					swipeToSlide: true,
					adaptiveHeight: true,
					slidesToScroll: 1,
					// centerMode: false,
					appendDots: $wrap.find('.slider-control__dots'),
					mobileFirst: true,

					responsive: [
						{
							breakpoint: 1260,
							settings: {
								dots: true
							}
						}
					]
				});

		} else {

			$slider
				.on('init', function (event, slick) {
					$wrap.find('.slider-control__count')
						.html((slick.currentSlide+1) + '/' + slick.slideCount);

					if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');

					if ( slick.slideCount > slick.options.slidesToShow){
						$control.removeClass('is-hidden');
					} else {
						$control.addClass('is-hidden');
					}
				})
				.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
					$wrap.find('.slider-control__count')
						.html((nextSlide+1) + '/' + slick.slideCount);
				})
				.on('breakpoint', function (event, slick) {
					if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');
					if ( slick.slideCount > slick.options.slidesToShow){
						$control.removeClass('is-hidden');
					} else {
						$control.addClass('is-hidden');
					}
				})
				.slick({
					dots: false,
					infinite: false,
					speed: 300,
					prevArrow: $wrap.find('.slider-control__arrow_prev'),
					nextArrow: $wrap.find('.slider-control__arrow_next'),
					variableWidth: true,
					swipeToSlide: true,
					adaptiveHeight: true,
					slidesToScroll: 1,
					// centerMode: false,
					appendDots: $wrap.find('.slider-control__dots'),
					mobileFirst: true,

					responsive: [
						{
							breakpoint: 1260,
							settings: {
								dots: true
							}
						}
					]
				});

			// image gallery

			let $group = $(this);

			if ( !$group.attr('data-viewerjs') ){
				$group.attr('data-viewerjs', true);

				let viewer = new Viewer($group[0], {
					url: 'data-original',
					button: false,
					navbar: false,
					rotatable: true,
					scalable: true,
					transition: false,
					fullscreen: true,
					keyboard: true,
					title: false,
					toolbar: false,
					tooltip: false,
					movable: false,
					zoomable: false,
					hidden: function (e) {
						document.getElementsByTagName('body')[0].style.paddingRight = '';
					},

					viewed: function (e) {
						// console.log(e);

						let $control = $('<div class="slider-control"><div class="slider-control__close"></div><div class="slider-control__arrow slider-control__arrow_prev"></div><div class="slider-control__arrow slider-control__arrow_next"></div></div>').insertAfter($(e.detail.image));
						let $frame = $('<div class="viewer-frame"></div>').insertAfter($(e.detail.image));

						$control.css({
							width: e.detail.image.offsetWidth,
							height: e.detail.image.offsetHeight,
							top: e.detail.image.offsetTop
						});

						$frame.css({
							width: e.detail.image.offsetWidth,
							height: e.detail.image.offsetHeight,
							top: e.detail.image.offsetTop
						});

						$control
							.on('click', '.slider-control__close', ()=>{
								viewer.hide();
							})
							.on('click', '.slider-control__arrow_prev', ()=>{
								viewer.prev(true);
							})
							.on('click', '.slider-control__arrow_next', ()=>{
								viewer.next(true);
							})
						;

						document.getElementsByTagName('body')[0].style.paddingRight = scrollBarWidth() + 'px';
					}
				});
			}

		}

	});

	function methodSlick(destroy) {

		$('.method__list').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent()
			;

			if ( destroy ){

				try{ $slider.slick('unslick');} catch (e){}

			} else {
				$slider
					.on('init', function (event, slick) {

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

					})
					.slick({
						dots: true,
						infinite: false,
						speed: 300,
						swipeToSlide: false,
						arrows: false,
						slidesToShow: 1,
						slidesToScroll: 1,
						adaptiveHeight: true,
						appendDots: $wrap.find('.method__count')
					});
			}

		});

	}

	function whoSlick(destroy) {

		$('.who__list').each(function () {

			let $slider = $(this),
				$wrap = $slider.parent(),
				$control = $wrap.find('.slider-control')
			;

			if ( destroy ){

				try{ $slider.slick('unslick');} catch (e){}

			} else {
				$slider
					.on('init', function (event, slick) {
						$wrap.find('.slider-control__count')
							.html((slick.currentSlide+1) + '/' + slick.slideCount);
						if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');

					})
					.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
						$wrap.find('.slider-control__count')
							.html((nextSlide+1) + '/' + slick.slideCount);
					})
					.on('breakpoint', function (event, slick) {
						if ( $control.hasClass('slider-control_aside') ) slick.$list.addClass('slick-overflow');
					})
					.slick({
						dots: false,
						infinite: false,
						swipeToSlide: true,
						speed: 300,
						prevArrow: $control.find('.slider-control__arrow_prev'),
						nextArrow: $control.find('.slider-control__arrow_next'),
						variableWidth: true,
						slidesToScroll: 1
					});
			}

		});

	}

	(function () {

		if ( !$('.js-height-100').length ) return false;

		let f = ()=>{
			let $section = $('.js-height-100'),
				$window = $(window)
			;

			$section.css({
				minHeight: window.innerHeight
			})
		};

		f();
		$(window).on('scroll resize', f);

	})();

});
